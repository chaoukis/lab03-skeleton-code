package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ActivityOne extends Activity {

    // string for logcat documentation
    private final static String TAG = "Lab-ActivityOne";

    private int onCreate;
    private int onStart;
    private int onResume;
    private int onStop;
    private int onRestart;
    private int onPause;
    private int onDestroy;
    //TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
    //TODO:  increment the variables' values when their corresponding lifecycle methods get called.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        //Log cat print out
        Log.i(TAG, "onCreate called");
        onCreate++;
        TextView tv = (TextView)findViewById(R.id.create);
        tv.setText(tv.getText()+ "" +onCreate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    // lifecycle callback overrides

    @Override
    protected void onStart() {
        super.onStart();

        //Log cat print out
        Log.i(TAG, "onStart called");
        onStart++;
        TextView tv = (TextView)findViewById(R.id.start);
        tv.setText(tv.getText()+ "" +onStart);
        //TODO:  update the appropriate count variable & update the view
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Log cat print out
        Log.i(TAG, "onStart called");
        onResume++;
        TextView tv = (TextView)findViewById(R.id.resume);
        tv.setText(tv.getText()+ "" +onResume);
        //TODO:  update the appropriate count variable & update the view
    }

    @Override
    protected void onStop(){
        super.onStop();

        //Log cat print out
        Log.i(TAG, "onStart called");
        onStop++;
        TextView tv = (TextView)findViewById(R.id.stop);
        tv.setText(tv.getText()+ "" +onStop);
        //TODO:  update the appropriate count variable & update the view
    }

    @Override
    protected void onRestart(){
        super.onRestart();

        //Log cat print out
        Log.i(TAG, "onStart called");
        onRestart++;
        TextView tv = (TextView)findViewById(R.id.restart);
        tv.setText(tv.getText()+ "" +onRestart);
        //TODO:  update the appropriate count variable & update the view
    }

    @Override
    protected void onPause(){
        super.onPause();

        //Log cat print out
        Log.i(TAG, "onStart called");
        onPause++;
        TextView tv = (TextView)findViewById(R.id.pause);
        tv.setText(tv.getText()+ "" +onPause);
        //TODO:  update the appropriate count variable & update the view
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        //Log cat print out
        Log.i(TAG, "onStart called");
        onDestroy++;
        TextView tv = (TextView)findViewById(R.id.destroy);
        tv.setText(tv.getText()+ "" +onDestroy);
        //TODO:  update the appropriate count variable & update the view
    }


    // TODO: implement 5 missing lifecycle callback methods

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        //TODO:  save state information with a collection of key-value pairs & save all  count variables
    }


    public void launchActivityTwo(View view) {
        startActivity(new Intent(this, ActivityTwo.class));
    }


}
